# gitlab 宪法编辑教程

首先，先注册一个 gitlab 账户，这应该不用教了吧。

然后打开 https://gitlab.com/simulating-china-ii/law/blob/master/constitution.md ，应该是这样的

![](gitlab_edit_1.png)

选择右上角的 Web IDE

然后就可以在右边编辑了

![](gitlab_edit_2.png)

提交后右边是新旧版本的比较，看左边

![](gitlab_edit_3.png)

然后就是提交 merge request，往下翻

![](gitlab_edit_4.png)

之后就提交好了。